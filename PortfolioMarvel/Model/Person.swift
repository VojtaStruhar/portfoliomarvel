//
//  Person.swift
//  PortfolioMarvel
//
//  Created by Vojtěch Struhár on 20/02/2020.
//  Copyright © 2020 Vojtěch Struhár. All rights reserved.
//

import Foundation

struct Person: Codable, Hashable {
    let id:             Int
    let name:           String
    let thumbnailUrl:   String
}

//
//  Character.swift
//  PortfolioMarvel
//
//  Created by Vojtěch Struhár on 19/02/2020.
//  Copyright © 2020 Vojtěch Struhár. All rights reserved.
//

import Foundation

struct HeroDataWrapper: Decodable {
//    let code:               Int
//    let status:             String
    let copyright:          String
    let attributionText:    String
//    let attributionHTML:    String
    let data:               HeroDataContainer
//    let etag:               String
}

struct HeroDataContainer: Decodable {
    let offset:         Int
    let limit:          Int
    let total:          Int
    let count:          Int
    let results:        [Hero]
}

struct Hero: Decodable {
    let id:             Int
    let name:           String
    let description:    String
    let modified:       Date
    let resourceURI:    String
    let urls:           [HeroUrl]
    let thumbnail:      HeroThumbnail
//    let comics:         ComicsList
//    let stories:        StoriesList
//    let events:         EventList
//    let series:         SeriesList
}

struct HeroUrl: Decodable {
    let type:           String
    let url:            String
}

struct HeroThumbnail: Decodable {
    let path:           String
    let `extension`:    String
}

struct ComicsList: Decodable {
    
    let available:      Int
    let returned:       Int
    let collectionURI:  String
    let items:          [ComicItem]
}

struct ComicItem: Decodable {
    let resourceURI:    String
    let name:           String
}

struct StoriesList: Decodable {
    let available:      Int
    let returned:       Int
    let collectionURI:  String
    let items:          [StoryItem]
}

struct StoryItem: Decodable{
    let resourceURI:    String
    let name:           String
    let type:           String
}
struct EventList: Decodable {
    let available:      Int
    let returned:       Int
    let collectionURI:  String
    let items:          [EventSummary]
}

struct EventSummary: Decodable {
    let resourceURI:    String
    let name:           String
    
}

struct SeriesList: Decodable {
    let available:      Int
    let returned:       Int
    let collectionURI:  String
    let items:          [SeriesSummary]
}

struct SeriesSummary: Decodable {
    let resourceURI:    String
    let name:           String
}

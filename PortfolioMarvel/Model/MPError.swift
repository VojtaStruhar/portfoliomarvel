//
//  MPError.swift
//  PortfolioMarvel
//
//  Created by Vojtěch Struhár on 20/02/2020.
//  Copyright © 2020 Vojtěch Struhár. All rights reserved.
//

import Foundation

enum MPError: String, Error {
    case unableToComplete           = "Unable to complete your request. Please, check your internet connection."
    case invalidResponse            = "Invalid response from the server. Please try again."
    case invalidData                = "The data received from the server was invalid. Please try again."
}

//
//  UIViewController+Ext.swift
//  PortfolioMarvel
//
//  Created by Vojtěch Struhár on 20/02/2020.
//  Copyright © 2020 Vojtěch Struhár. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func presentMPAlertOnMainThread(title:String, message: String, buttonTitle: String) {
        DispatchQueue.main.async {
            let alert = MPAlertVC(title: title, message: message, buttonTitle: buttonTitle)
            alert.modalPresentationStyle    = .overFullScreen
            alert.modalTransitionStyle      = .crossDissolve
            self.present(alert, animated: true)
        }
    }
    
    func showLoadingView() {
        let loadingView = MPLoadingView(frame: view.bounds)
        view.addSubview(loadingView)
        
        loadingView.animate()
    }
    
    @objc func dismissLoadingView() {
        DispatchQueue.main.async {
            // If alert (or something) is presented over the loading view, it will not disappear
            let subviews = self.view.subviews
            for subview in subviews {
                if let _ = subview as? MPLoadingView { subview.removeFromSuperview() }
            }
        }
    }
}

//
//  Constants.swift
//  PortfolioMarvel
//
//  Created by Vojtěch Struhár on 20/02/2020.
//  Copyright © 2020 Vojtěch Struhár. All rights reserved.
//

import UIKit

enum Images {
    static let placeholderHero = UIImage(named: "hero-placeholder")
}

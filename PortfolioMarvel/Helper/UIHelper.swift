//
//  UIHelper.swift
//  PortfolioMarvel
//
//  Created by Vojtěch Struhár on 20/02/2020.
//  Copyright © 2020 Vojtěch Struhár. All rights reserved.
//

import UIKit

struct UIHelper {
    static func createThreeColumnFlowLayout(in view: UIView) -> UICollectionViewFlowLayout {
        let width                       = view.frame.width
        let padding: CGFloat            = 12
        let minimumItemSpacing: CGFloat = 10
        let availableWidth              = width - (padding * 2) - (minimumItemSpacing * 2)
        let itemWidth                   = availableWidth / 3
        
        let layout                      = UICollectionViewFlowLayout()
        layout.sectionInset             = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        layout.itemSize                 = CGSize(width: itemWidth, height: itemWidth + 40) // extra space for label under the image
        
        return layout
    }
}

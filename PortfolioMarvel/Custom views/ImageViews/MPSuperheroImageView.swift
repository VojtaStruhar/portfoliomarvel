//
//  MPSuperheroImageView.swift
//  PortfolioMarvel
//
//  Created by Vojtěch Struhár on 20/02/2020.
//  Copyright © 2020 Vojtěch Struhár. All rights reserved.
//

import UIKit

class MPSuperheroImageView: UIImageView {

    let placeholderImage = Images.placeholderHero
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func configure() {
        layer.cornerRadius  = 10
        clipsToBounds       = true
        image               = placeholderImage
        contentMode         = .scaleAspectFill
        backgroundColor     = .tertiarySystemBackground
        translatesAutoresizingMaskIntoConstraints = false
    }
}

//
//  MPLoadingView.swift
//  PortfolioMarvel
//
//  Created by Vojtěch Struhár on 20/02/2020.
//  Copyright © 2020 Vojtěch Struhár. All rights reserved.
//

import UIKit

class MPLoadingView: UIView {

    let indicator = UIActivityIndicatorView(style: .large)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
 
    func configure() {
        addSubview(indicator)
        backgroundColor = .systemBackground
        alpha = 0
        
        indicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            indicator.centerYAnchor.constraint(equalTo: centerYAnchor),
            indicator.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
    func animate() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.25) { self.alpha = 0.8 }
            self.indicator.startAnimating()
        }
    }
}

//
//  MPAlertVC.swift
//  PortfolioMarvel
//
//  Created by Vojtěch Struhár on 20/02/2020.
//  Copyright © 2020 Vojtěch Struhár. All rights reserved.
//

import UIKit

class MPAlertVC: UIViewController {

    let containerView   = MPAlertContainerView()
    let titleLabel      = MPTitleLabel(textAlignment: .center, fontSize: 21)
    let messageLabel    = MPBodyLabel(textAlignment: .center)
    let dismissButton   = MPButton(backgroundColor: .systemRed, title: "Ok")
    
    var titleMessage: String?
    var message: String?
    var buttonTitle: String?
    
    let padding: CGFloat = 16
    
    // MARK: - Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewController()
        configureContainerView()
        configureTitleLabel()
        configureMessageLabel()
        configureDismissButton()
    }
    
    init(title: String, message: String, buttonTitle: String) {
        super.init(nibName: nil, bundle: nil)
        
        self.titleMessage   = title
        self.message        = message
        self.buttonTitle    = buttonTitle
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViewController() {
        view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
    }
    
    func configureContainerView() {
        view.addSubview(containerView)
        
        NSLayoutConstraint.activate([
            containerView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            containerView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            containerView.widthAnchor.constraint(equalToConstant: 280)
        ])
    }
    
    func configureTitleLabel() {
        containerView.addSubview(titleLabel)
        titleLabel.text = titleMessage ?? "Error"
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: padding),
            titleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: padding),
            titleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -padding)
        ])
    }
    
    func configureMessageLabel() {
        containerView.addSubview(messageLabel)
        messageLabel.text = message
        
        NSLayoutConstraint.activate([
            messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: padding),
            messageLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: padding),
            messageLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -padding),
            messageLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 150)
        ])
    }
    
    func configureDismissButton() {
        containerView.addSubview(dismissButton)
        dismissButton.setTitle(buttonTitle ?? "Ok", for: .normal)
        dismissButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        
        NSLayoutConstraint.activate([
            dismissButton.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: padding),
            dismissButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: padding),
            dismissButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -padding),
            dismissButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -padding),
            dismissButton.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    @objc func dismissVC() {
        self.dismiss(animated: true)
    }
}

//
//  CharacterCell.swift
//  PortfolioMarvel
//
//  Created by Vojtěch Struhár on 20/02/2020.
//  Copyright © 2020 Vojtěch Struhár. All rights reserved.
//

import UIKit

class CharacterCell: UICollectionViewCell {
    
    static let reuseID              = "CharacterCell"
    private let padding: CGFloat    = 8
    private let imageView           = MPSuperheroImageView(frame: .zero)
    private let nameLabel           = MPTitleLabel(textAlignment: .center, fontSize: 17)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureImageView()
        configureNameLabel()
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    func set(person: Person) {
        nameLabel.text  = person.name
        imageView.image = Images.placeholderHero // set placeholder back after recycling the cell
        NetworkingManager.shared.downloadImage(from: person.thumbnailUrl) { [weak self] (image) in
            guard let self = self else { return }
            DispatchQueue.main.async { self.imageView.image = image }
        }
    }
    
    private func configureImageView() {
        addSubview(imageView)
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: padding),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -padding),
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor)
        ])
    }
    
    private func configureNameLabel() {
        addSubview(nameLabel)
        nameLabel.numberOfLines = 2
        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: padding),
            nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -padding)
//            nameLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: padding)
        ])
    }
}

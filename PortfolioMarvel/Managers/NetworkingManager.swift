//
//  NetworkingManager.swift
//  PortfolioMarvel
//
//  Created by Vojtěch Struhár on 20/02/2020.
//  Copyright © 2020 Vojtěch Struhár. All rights reserved.
//

import UIKit

struct Parameter {
    let key:    String
    let value:  String
}

class NetworkingManager {
    
    static let shared       = NetworkingManager()

    let cache               = NSCache<NSString, UIImage>()
    let apiKey              = "0e2582d2556092e59a41750f96ca1402"
    let baseUrl             = "http://gateway.marvel.com:80/v1/public/"
    let charactersPerPage   = 40
    
    func getCharacters(page: Int, completion: @escaping(Result<[Person], MPError>) -> Void) {
        
        let request = createURLRequest(with: baseUrl + "characters", parameters: [
            Parameter(key: "apikey",    value: apiKey),
            Parameter(key: "offset",    value: String((page - 1) * charactersPerPage)),
            Parameter(key: "limit",     value: String(charactersPerPage))
        ])
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let _ = error {
                completion(.failure(.unableToComplete))
                return
            }

            guard let data = data else {
                completion(.failure(.invalidData))
                return
            }
            do {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy    = .iso8601
                let decoded = try decoder.decode(HeroDataWrapper.self, from: data)
                
                var result: [Person] = []
                for hero in decoded.data.results {
                    result.append( Person(id: hero.id, name: hero.name, thumbnailUrl: hero.thumbnail.path + "." + hero.thumbnail.extension) )
                }
                completion(.success(result))
            } catch let badStuff {
                print("badStuff:\n",badStuff)
                completion(.failure(.invalidData))
            }
        }
        task.resume()
    }
    
    private func createURLRequest(with urlString: String, parameters: [Parameter]) -> URLRequest {
        var endpoint = urlString + (parameters.isEmpty ? "" : "?")
        for param in parameters {
            endpoint += param.key + "=" + param.value + "&"
        }
        var request = URLRequest(url: URL(string: endpoint)!)
        request.addValue("cz.vojtechstruhar.PortfolioMarvel", forHTTPHeaderField: "Referer")
        request.addValue("application/json", forHTTPHeaderField: "Content")
        return request
    }
    
    func downloadImage(from urlString: String, completion: @escaping (UIImage?) -> Void) {
        let cacheKey = NSString(string: urlString)
        if let image = cache.object(forKey: cacheKey) {
            // if there is cached image, return it
            completion(image)
            return
        }
        
        guard let url = URL(string: urlString) else { return }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard error == nil,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200,
                let data = data,
                let image = UIImage(data: data) else {
                    completion(nil)
                    return
            }
            
            self.cache.setObject(image, forKey: cacheKey)
            DispatchQueue.main.async {
                completion(image)
            }
            
        }
        task.resume()
    }
    
}

//
//  CharacterListVC.swift
//  PortfolioMarvel
//
//  Created by Vojtěch Struhár on 19/02/2020.
//  Copyright © 2020 Vojtěch Struhár. All rights reserved.
//

import UIKit

class CharacterListVC: UIViewController {

    enum Section { case main } // only 1 section
    
    var dataSource: UICollectionViewDiffableDataSource<Section, Person>!
    var collectionView: UICollectionView!
    var characters: [Person] = []
    
    var currentPage         = 5
    var isThereMoreHeroes   = true
    var isLoadingMoreHeroes = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        
        configureCollectionView()
        configureDataSource()
        getCharacters(page: currentPage)
    }

    func getCharacters(page: Int) {
        showLoadingView()
        isLoadingMoreHeroes = true
        
        let manager = NetworkingManager.shared
        manager.getCharacters(page: page) { [weak self] (result) in
            guard let self = self else { return }
            self.dismissLoadingView()
            self.isLoadingMoreHeroes = false
            
            switch result {
            case .success(let people):
                if people.count < manager.charactersPerPage { self.isThereMoreHeroes = false }
                
                self.characters.append(contentsOf: people)
                self.updateData(on: self.characters)
                
            case .failure(let error):
                print(error.rawValue)
            }
        }
    }

    func configureCollectionView() {
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: UIHelper.createThreeColumnFlowLayout(in: view))
        collectionView.delegate = self
        view.addSubview(collectionView)
        collectionView.backgroundColor = .systemBackground
        collectionView.register(CharacterCell.self, forCellWithReuseIdentifier: CharacterCell.reuseID)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    func configureDataSource() {
        dataSource = UICollectionViewDiffableDataSource<Section, Person>(collectionView: collectionView, cellProvider: { (collectionView, indexPath, person) -> UICollectionViewCell? in
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CharacterCell.reuseID, for: indexPath) as! CharacterCell
            cell.set(person: person)
            return cell
        })
    }
    
    func updateData(on people: [Person]) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, Person>()
        snapshot.appendSections([.main])
        snapshot.appendItems(people)
        DispatchQueue.main.async {
            self.dataSource.apply(snapshot, animatingDifferences: true)
        }
    }
}

extension CharacterListVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let person = characters[indexPath.row]
        
        presentMPAlertOnMainThread(title: person.name, message: "Character detail page WIP", buttonTitle: ":)")
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let offsetY         = scrollView.contentOffset.y
        let contentHeight   = scrollView.contentSize.height
        let height          = scrollView.frame.size.height
        
        if offsetY > contentHeight - height {
            guard isThereMoreHeroes, !isLoadingMoreHeroes else {
                print("No more heroes to load!")
                return }
            currentPage += 1
            getCharacters(page: currentPage)
        }
    }
}
